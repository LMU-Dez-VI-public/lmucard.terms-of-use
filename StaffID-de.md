## Nutzungsbedingungen, Datenverwendungshinweise und Datenschutzinformationen für die LMUcard für Beschäftigte (Dienstausweis)

### Allgemeine Hinweise zur LMUcard für Beschäftigte (Dienstausweis) und zum Zweck der Datenverarbeitung

Die LMUcard für Beschäftigte (Dienstausweis) ist eine multifunktionale Chipkarte mit folgenden Funktionen:

- Dienstausweis
- Bargeldloses Bezahlen in den Gastronomiebetrieben des Studentenwerks sowie an Druckern, Scannern und Kopierern, die mit dem Zahlungssystem kompatibel sind.
- Bibliotheksausweis für die Universitätsbibliothek und die Bayerische Staatsbibliothek (optional)

Die Karte enthält einen optisch wiederbeschreibbaren Validierungsstreifen, auf dem Informationen zur Gültigkeit vermerkt sind. Zur Aktualisierung der Gültigkeit muss die LMUcard von den Karteninhabern validiert werden. Hinweise hierzu und zu den weiteren Funktionen der LMUcard werden unter www.lmu.de/lmucard/dienstausweis im Serviceportal der LMU veröffentlicht.
Zwecke der Datenverarbeitung sind die Erstellung und Nutzung der LMUcard als multifunktionale Chipkarte. Eine Datenverarbeitung zu anderen als zu den angegebenen bzw. gesetzlich zugelassenen Zwecken (z.B. zur internen Überprüfung der Sicherheitssysteme und zur Gewährleistung der internen Netz- und Informationssicherheit gemäß Art. 6 Abs. 1 BayDSG) erfolgt nicht.
Wir verarbeiten nur die personenbezogenen Daten, die Sie uns zur Verfügung gestellt haben oder die wir im Rahmen der Tätigkeit an der LMU über Sie in zulässiger Weise erhalten haben, Art. 4 Abs. 2 BayDSG).

### Rechtliche Grundlagen

Wir verarbeiten Ihre Daten im Einklang mit und auf Basis der Datenschutz-Grundverordnung (DSGVO), des Bayerischen Datenschutzgesetzes (BayDSG) und der sonstigen anwendbaren Datenschutzbestimmungen.

Die Verarbeitung der personenbezogenen Daten erfolgt im Rahmen der Erstellung eines Dienstausweises gemäß Art. 6 Abs. 1 lit b) DSGVO (Beschäftigungskontext) sowie Art. 6 Abs. 1 lit. e), Abs. 2, 3 DSGVO i.V.m. Art. 4 Abs. 1 BayDSG i.V.m. § 35 Bayerische Allgemeine Geschäftsordnung (BayAGO) (Aufgabenerfüllung). Darüber hinaus gilt für die Nutzung als Bibliotheksausweis Art. 6 Abs. 1 lit. b) DSGVO sowie die Allgemeine Benützungsordnung der Bayerischen Staatlichen Bibliotheken (BayABOB). Die Datenverarbeitung im Rahmen der Nutzung als Mensakarte basiert auf Art. 6 Abs. 1 lit. b) DSGVO sowie Art. 6 Abs. 1 lit. e), Abs. 2, 3 DSGVO i.V.m. Art. 4 Abs. 1 BayDSG i.V.m. Art. 88 BayHSchG. Die Ausstellung der LMUcard basiert auf Art. 6 Abs. 1 lit. a DSGVO (Einwilligung).

### Verarbeitung von personenbezogenen Daten

Um die Funktionen der LMUcard zu ermöglichen, sind folgende personenbezogene Daten auf der Karte aufgebracht:

- Foto
- Akademischer Grad
- Name
- Vorname
- Kartennummer
- Bibliotheksnummer
- Gültigkeitsvermerk (gültig-bis-Datum)

Auf der Karte befindet sich ein RFID-Chip mit Datenspeicher, auf dem folgende Merkmale in unterschiedlichen Segmenten gespeichert sind:

- Personalnummer
- Kartentyp („Mitarbeiter")
- Kartennummer
- Bibliotheksnummer
- Gültigkeitsvermerk (gültig-bis-Datum)
- eduPersonPrincipalName (LMU-ID@lmu.de)

Die gespeicherten Daten sind zugriffsgeschützt und können nur mit entsprechend konfigurierten Kartenlesern ausgelesen werden. Die Nutzung der gespeicherten Daten unterliegt den einschlägigen rechtlichen Vorgaben, insbesondere zum Datenschutz und zur Datensicherheit.
Im Rahmen der LMUcard-Verwaltung werden folgende weitere Daten auf Servern der LMU unter der Administration des Dezernats VI und unter Beachtung der geeigneten technischen und organisatorischen Sicherheitsmaßnahmen gespeichert:

- Daten zu beantragten Karten (Datum des Antragsdaten sowie die o.g. Daten)
- Daten zu bereits ausgestellten Karten

Die Daten sind für die Nutzung der LMUcard erforderlich. Der Upload eines Fotos ist verpflichtend. Ohne Foto ist die Ausstellung und Nutzung der LMUcard als Dienstausweis nicht möglich.
Die Verarbeitung der Daten erfolgt auf internen Datenverarbeitungsanlagen. Innerhalb der LMU erhalten nur diejenigen Personen bzw. Bereiche Ihre personenbezogenen Daten, die diese für die Erfüllung der genannten Verarbeitungszwecke benötigen.
Ihre Daten werden nicht in rein automatisierten Verarbeitungsprozessen zur Herbeiführung einer Entscheidung verarbeitet.

### Datenübermittlung

Um die Funktionen der LMUcard entsprechend den o.g. Zwecken zu ermöglichen, müssen personenbezogene Daten an andere Einrichtungen übermittelt werden.

- An das Studentenwerk werden folgende Daten des Karteninhabers übermittelt:
  - Kartennummer
  - Kartentyp („Student")
  - Gültigkeitsvermerk (gültig-bis-Datum)
- Für den Fall, dass Sie nach der Beantragung der LMUcard die Bibliotheksfunktion aktiviert haben, werden folgende Daten an die Universitätsbibliothek und an die Bayerische Staatsbibliothek übermittelt:
  - Bibliotheksnummer
  - Bibliotheksbenutzergruppe
  - LMU-ID
  - Name
  - Vorname
  - Geschlecht
  - Akademischer Grad
  - Geburtsdatum
  - LMU-E-Mailadresse
  - Private E-Mailadresse
  - Dienstadresse
  - Telefonnummer/Mobilfunknummer
  - Staatsangehörigkeit
  - Organisationseinheit (bspw. Fakultät)
  - Kartennummer
  - Status der Karte (in Besitz, verloren)
  - Gültigkeitsvermerk (gültig-bis-Datum)

Die Weitergabe ist für die Aktivierung der Karte als Bibliotheksausweis und für die Begründung des entsprechenden Nutzungsverhältnisses erforderlich.

### Dauer der Speicherung der personenbezogenen Daten

Die gespeicherten Daten werden verarbeitet, solange und soweit dies für den Zweck der Datenverarbeitung im Rahmen der LMUcard erforderlich ist (Art. 4 Abs. 1 BayDSG). Nach Rückgabe der LMUcard werden die Daten unverzüglich aus den entsprechenden Systemen gelöscht, sofern und soweit dem keine gesetzlichen Aufbewahrungspflichten entgegenstehen. Das gilt ggf. auch für die beim Studentenwerk, der Universitätsbibliothek und der Bayerischen Staatsbibliothek gespeicherten Daten, soweit kein offenes Nutzungsverhältnis mehr besteht oder gesetzliche Aufbewahrungspflichten bestehen. Das hochgeladene Foto wird sieben Tage nach Aushändigung der LMUcard gelöscht.

### Gebühren

Die Ausstellung der LMUcard ist für die Beschäftigten kostenlos. Für die Ausstellung einer Ersatzkarte (bspw. bei Beschädigung oder Verlust) kann eine Bearbeitungsgebühr erhoben werden.

### Verantwortlicher, zuständige Dienststelle und Support

#### Verantwortlicher für die Datenverarbeitung

<address>
Ludwig-Maximilians-Universität München
gesetzlich vertreten durch den Präsidenten, Herrn Prof. Dr. Bernd Huber,
Geschwister-Scholl-Platz 1
80539 München
</address>

#### Zuständige Dienststelle für die Datenverarbeitung

<address>
Ludwig-Maximilians-Universität München<br/>
Dezernat VI - Informations- und Kommunikationstechnik<br/>
Geschwister-Scholl-Platz 1<br/>
80539 München
</address>
<VI@verwaltung.uni-muenchen.de>

#### Allgemeine Fragen und Support

<address>
Ludwig-Maximilians-Universität München<br/>
Dezernat VI - Informations- und Kommunikationstechnik<br/>
-- IT-Servicedesk --<br/>
Geschwister-Scholl-Platz 1<br/>
80539 München<br/>
</address>
<it-servicedesk@lmu.de>

### Kontaktdaten des behördlichen Datenschutzbeauftragten der LMU

<address>
Ludwig-Maximilians-Universität München<br/>
-- Behördlicher Datenschutzbeauftragter --<br/>
Geschwister-Scholl-Platz 1<br/>
80539 München <br/>
Tel.: 089-2180-2414<br/>
https://www.lmu.de/datenschutz<br/>
</address>
<datenschutz@verwaltung.uni-muenchen.de>

### Information zu bestehenden Rechten

Sie haben das Recht, Auskunft über die zu Ihrer Person gespeicherten Daten zu erhalten (Art. 15 DSGVO). Sollten unrichtige personenbezogene Daten verarbeitet werden, steht Ihnen ein Recht auf Berichtigung zu (Art. 16 DSGVO). Zudem haben Sie das Recht auf Löschung (Art. 17 DSGVO), Widerspruch (Art. 21 DSGVO), Beschränkung (Art. 18 DSGVO) und auf Widerruf einer Einwilligung für die Zukunft. Die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Datenverarbeitung wird durch diesen nicht berührt.

Daneben steht Ihnen gemäß Art. 77 DSGVO das Recht zur Beschwerde bei einer Datenschutz-Aufsichtsbehörde zu.

Die für die Ludwig-Maximilians-Universität München zuständige Datenschutz-Aufsichtsbehörde ist der Bayerische Landesbeauftragte für den Datenschutz (Postfach 22 12 19, 80502 München, Telefon: 089 212672-0, Telefax: 089 212672-50, E-Mail: poststelle@datenschutz-bayern.de, Internet: www.datenschutz-bayern.de).

Sollten Sie von Ihren Rechten Gebrauch machen wollen oder haben Sie Fragen, wenden Sie sich bitte an die zuständige Dienststelle für die Datenverarbeitung (Ziffer 7). Diese prüft, ob die gesetzlichen Voraussetzungen hierfür erfüllt sind und trifft dann die erforderlichen Maßnahmen.

### Es gelten die Nutzungsbedingungen, Datenverwendungshinweise und Datenschutzinformationen in der jeweils geltenden Fassung

Diese sind abrufbar unter https://www.portal.uni-muenchen.de/benutzerkonto/index.html#!/LMUcard/about/StaffID/termsofuse

### Datenschutzrechtliche Einwilligungserklärung

Ich habe die Nutzungsbedingungen, Datenverwendungshinweise und Datenschutzinformationen zur Kenntnis genommen und bin mit deren Geltung einverstanden.
Mit der Datenverarbeitung für die vorbenannten Zwecke, insbesondere der Weitergabe der Daten an das Studentenwerk, die Universitätsbibliothek und die Bayerische Staatsbibliothek (bei aktivierter Bibliotheksausweis-Funktion), bin ich einverstanden.
Die Einwilligung ist freiwillig. Ohne Einwilligung ist die Ausstellung und Nutzung der LMUcard als Multifunktionskarte nicht möglich.
