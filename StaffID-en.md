## Nutzungsbedingungen und Datenverwendungshinweise für den Pilotbetrieb der LMUcard für Beschäftige (Dienstausweis)

### Hinweise zum Pilotbetrieb

Im Rahmen des Pilotbetriebs wird die Ausstellung und Nutzung der LMUcard für Beschäftigte erprobt.
Die ausgestellte LMUcard hat grundsätzlich vollen Funktionsumfang, mögliche Einschränkungen in der Nutzung sind allerdings nicht ausgeschlossen.
Der Pilotbetrieb wird durch einen entsprechenden Hinweis auf dem Validierungsstreifen (s.u.) auf der Karte sichtbar gemacht.

Ihre LMUcard fungiert zugleich als Bibliotheksausweis für die Universitätsbibliothek der LMU sowie für die Bayerische Staatsbibliothek. Die Bibliotheksfunktion Ihrer LMUcard wird durch die Universitätsbibliothek automatisch aktiviert; es ist also nicht notwendig, dass Sie für die Aktivierung in die UB kommen.
Wenn Sie bereits einen Bibliotheksausweis besitzen, wird Ihr vorhandener Bibliotheksausweis durch die UB auf Ihre LMUcard umgebucht. Ihr vorhandener Bibliotheksausweis verliert damit seine Gültigkeit, die LMUcard fungiert dann als Ihr Bibliotheksausweis.
Da wir uns noch in der Erprobungsphase der LMUcard befinden, kann die Aktivierung der Bibliotheksfunktion bzw. die Umbuchung Ihres vorhandenen Bibliotheksausweises einige Tage in Anspruch nehmen; hierfür bitten wir Sie um Ihr Verständnis.
Bei Fragen zur Bibliotheksfunktion Ihrer LMUcard steht Ihnen die Abteilung Benutzungsdienste der UB gerne zur Verfügung (E-Mail <benutzung@ub.uni-muenchen.de>, Tel. <a href="tel:+498921802429">089 / 2180-2429</a>).
Bitte beachten Sie, dass Sie Änderungen Ihrer Kontaktdaten bis auf Weiteres sowohl an die Zentrale Universitätsverwaltung als auch (über den OPAC) an die Universitätsbibliothek melden müssen.

Es ist geplant, dass die Karte nach Abschluss des Pilotbetriebs beim Karteninhaber verbleibt und nach erneuter Validierung (s.u.) regulär weitergenutzt werden kann.

Die folgenden Nutzungsbedingungen und Datenverwendungshinweise befinden sich noch im Aufbau und sind noch nicht abschließend durch den Datenschutzbeauftragten der LMU geprüft.

Wir freuen uns über Feedback zur Nutzung der Karte.
Bitte teilen Sie uns Ihre Erfahrungen mit: <it-servicedesk@lmu.de>

### Allgemeine Hinweise zur LMUcard für Beschäftigte (Dienstausweis)

Die LMUcard für Beschäftigte (Dienstausweis) ist eine multifunktionale
Chipkarte mit folgenden Funktionen:

- Dienstausweis
- Bibliotheksausweis für die Universitätsbibliothek und die Bayerische Staatsbibliothek
- Bargeldloses Bezahlen in den Gastronomiebetrieben des Studentenwerks

Die Karte enthält einen wiederbeschreibbaren Validierungsstreifen, auf dem das Gültigkeitsdatum vermerkt ist.
Zur Aktualisierung der Gültigkeit muss die LMUcard von den Karteninhabern validiert werden.
Hinweise hierzu und zu den weiteren Funktionen der LMUcard werden unter www.lmu.de/lmucard/dienstausweis im Serviceportal der LMU veröffentlicht.

### Anerkennung der rechtlichen Grundlagen
Rechtsgrundlage für den Dienstausweis ist §35 BayAGO, rechtlich maßgebend für die Nutzung als Bibliotheksausweis ist die Allgemeine Benützungsordnung der Bayerischen Staatlichen Bibliotheken (BayABOB).


### Gespeicherte Daten

Um die Funktionen der LMUcard zu ermöglichen, sind folgende Daten auf
der Karte aufgebracht:

- Foto
- Akad. Titel
- Name, Vorname
- Kartennummer
- ID-Nr. Studentenwerk
- Bibliotheksnummer
- gültig-bis-Datum

Auf der Karte befindet sich ein RFID-Chip mit Datenspeicher, auf dem folgende Merkmale in unterschiedlichen Segmenten gespeichert sind:

- Personalnummer
- Kartentyp (bei Dienstausweis immer „Mitarbeiter")
- Kartennummer
- gültig-bis-Datum
- eduPersonPrincipalName (LMU-ID@lmu.de)

Die gespeicherten Daten sind zugriffsgeschützt und können nur mit entsprechend konfigurierten Lesern ausgelesen werden.
Die Nutzung der gespeicherten Daten unterliegt den einschlägigen rechtlichen Vorgaben, insbes. des Datenschutzes.

Im Rahmen der LMUcard-Verwaltung werden folgende Daten auf Servern des Dezernats VI gespeichert:

- Daten zu beantragten Karten (Datum des Antrags und o.g. Daten)
- Daten zu bereits ausgestellten Karten

### Einwilligung in Datenübermittlung

Um die Funktionen der LMUcard zu ermöglichen, werden folgende Daten
übermittelt:

- An die Universitätsbibliothek werden für die elektronische Ausleihe folgende Daten des Karteninhabers übermittelt:
  - Name
  - Vorname
  - Geburtsdatum
  - Personalnummer
  - erste und zweite E-Mailadresse
  - Dienstadresse
  - Telefonnummer
  - Staatsangehörigkeit
  - Benutzergruppe
  - Organisationseinheit (bspw. Fakultät)
  - Kartennummer
  - Bibliotheksnummer
  - gültig-bis-Datum

Die Universitätsbibliothek stellt diese Daten der Bayerischen Staatsbibliothek zu Verfügung.
Im Falle eines Mahnverfahrens wird der Universitätsbibliothek sowie der Bayerischen Staatsbibliothek auf Anfrage die Privatanschrift übermittelt.

- Das Studentenwerk erhält folgende Daten des Karteninhabers:
  - Kartennummer
  - Kartentyp („Mitarbeiter")
  - gültig-bis-Datum

### Dauer der Speicherung der personenbezogenen Daten

Die gespeicherten Daten werden nach Ende des Beschäftigungsverhältnisses unter Einhaltung gesetzlicher Löschfristen gelöscht.

### Rückgabe der Karte

Gemäß §35 BayAGO sind die Beschäftigten verpflichtet, den Dienstausweis bei Beendigung des Beschäftigungsverhältnisses zurückzugeben.

### Gebühren

Die Ausstellung der LMUcard ist kostenlos.
Für die Ausstellung einer Ersatzkarte (bspw. bei Verlust) wird zukünftig u.U. eine Bearbeitungsgebühr erhoben.

### Ansprechpartner und Kontakt

#### Verantwortlich

<address>
Ludwig-Maximilians-Universität München<br/>
Dezernat VI - Informations- und Kommunikationstechnik<br/>
Dr. Oliver Diekamp<br/>
Geschwister-Scholl-Platz 1<br/>
80539 München
</address>
<VI@verwaltung.uni-muenchen.de>

#### Kontaktdaten des behördlichen Datenschutzbeauftragten der LMU
<address>
Herr Dr. jur. Rolf Gemmeke<br/>
Ludwig-Maximilians-Universität München<br/>
-- Behördlicher Datenschutzbeauftragter --<br/>
Geschwister-Scholl-Platz 1<br/>
80539 München
</address>
<datenschutz@verwaltung.uni-muenchen.de>

#### Allgemeine Fragen und Support

<address>
Ludwig-Maximilians-Universität München<br/>
Dezernat VI - Informations- und Kommunikationstechnik<br/>
-- IT-Servicedesk --<br/>
Geschwister-Scholl-Platz 1<br/>
80539 München<br/>
</address>
<it-servicedesk@lmu.de>

### Datenschutzrechtliche Erklärung und Einwilligung

Ich habe die Nutzungsbedingungen und Datenverwendungshinweise zur Kenntnis genommen und bin mit deren Geltung einverstanden.
