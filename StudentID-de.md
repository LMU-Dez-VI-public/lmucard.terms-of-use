## Nutzungshinweise und Datenschutzinformationen für die LMUcard für Studierende (Studierendenausweis)

### Allgemeine Hinweise zur LMUcard für Studierende (Studierendenausweis) und zum Zweck der Datenverarbeitung

Die LMUcard für Studierende (Studierendenausweis) ist eine multifunktionale Chipkarte mit folgenden Funktionen:

- Sichtausweis als Studierendenausweis
- MVV-Fahrkarte (Semesterticket Basis)
- Bargeldloses Bezahlen in den Gastronomiebetrieben des Studentenwerks sowie an Druckern, Scannern und Kopierern, die mit dem Zahlungssystem kompatibel sind.
- Bibliotheksausweis für die Universitätsbibliothek und die Bayerische Staatsbibliothek

Die Karte enthält einen optisch wiederbeschreibbaren Validierungsstreifen, auf dem Informationen zur Gültigkeit, dem Semesterticket und die Fakultät vermerkt sind. Zur Aktualisierung der Gültigkeit muss die LMUcard von den Karteninhabern validiert werden. Hinweise hierzu und zu den weiteren Funktionen der LMUcard werden unter www.lmu.de/studierendenausweis veröffentlicht.

Die LMUcard ist an den Anforderungen an den Internationalen Studierendenausweis des ISIC angelehnt konzipiert und umfasst die gleichen Stammdaten wie sie dort von den Studierenden gefordert werden.

Das Recht zur Ausstellung der LMUcard folgt aus dem Selbstverwaltungsrecht der Universität München in Ausprägung ihres Organisationsrechts (Art. 12 Abs. 1 BayHSchG).
Die LMUcard ist gem. § 6 Abs. 3 der Immatrikulations-, Rückmelde- und Exmatrikulationssatzung der Ludwig-Maximilians-Universität München ein auf Antrag ausgestellter Studierendenausweis und als solcher optional. Die Beantragung und Nutzung ist freiwillig.

Zwecke der Datenverarbeitung sind die Erstellung und Nutzung der LMUcard als multifunktionale Chipkarte.
Mittels LMUcard können Studierende zum einen ihre Immatrikulation (Art. 42 Abs. 2 BayHSchG), ihren Studierendenstatus und damit ihre Mitgliedschaft zur Universität München und zu einer ihrer Fakultäten im Rechtsverkehr – d.h. innerhalb der Universität und ihrer Einrichtungen sowie gegenüber Dritten - nachweisen (Art. 11 Abs. 1 BayHSchG, Art. 17 Abs. 1 Satz 1 BayHSchG, Art. 27 Abs. 2 BayHSchG).
Zum anderen besteht die Möglichkeit der Nutzung als Bibliotheksausweis und Mensakarte und für das Semesterticket.

### Verarbeitung von personenbezogenen Daten und rechtliche Grundlagen

Wir verarbeiten Ihre Daten im Einklang mit und auf Basis der Datenschutz-Grundverordnung (DSGVO), des Bayerischen Datenschutzgesetzes (BayDSG) und der sonstigen anwendbaren Datenschutzbestimmungen.

Um die Funktionen der LMUcard zu ermöglichen, sind folgende personenbezogene Daten auf der Karte aufgebracht:

- Permanent (Stammdaten)
  - Akademischer Titel (optional)
  - Name
  - Vorname
  - Matrikelnummer
  - Geburtsdatum
  - Foto (optional)
  - Unterschrift auf der Rückseite
- Kartendaten (vom Rohling der LMUcard)
  - Kartennummer
  - Bibliotheksnummer
- Temporär (optisch wiederbeschreibbarer Validierungsstreifen (Thermo-Read-Write-Streifen))
  - Gültigkeitsvermerk (gültig-bis-Datum)
  - Fakultät (in der Regel die Fakultät, welche dem ersten Fach des ersten Studiengangs zugeordnet ist. Nähere Informationen zur Bestimmung der Fakultät sind im Anhang 2 der Grundordnung der LMU enthalten).
  - Semesterticket Basis (siehe https://www.mvv-muenchen.de/tickets/zeitkarten-abos/mvv-semesterticket/index.html)

Der Datenverarbeitung liegen folgende Rechtsgrundlagen und Zwecke zugrunde:

<table>
<thead>
<tr>
<th>Datum</th>
<th>Rechtsgrundlage</th>
<th>Zweck</th>
</tr>
</thead>
<tbody>
<tr>
<th colspan="3">Permanent aufgebrachte Stammdaten</th>
</tr><tr>
  <th>Foto</th>
  <td>Art. 6 Abs. 1 lit. a DSGVO</td>
  <td>Identifikation und Ausweis, Erfüllung der Anforderungen an einen internationalen Studierendenausweis (ISIC)</td>
</tr><tr>
  <th>Akademischer Titel</th>
  <td>Art. 6 Abs. 1 lit. a DSGVO;<br/>
      Art. 6 Abs. 1 lit. e DSGVO i.V.m Art. 4 Abs.1 BayDSG </td>
  <td>Identifikation und Ausweis, Erfüllung der Anforderungen an einen internationalen Studierendenausweis (ISIC), ggf. Namensbestandteil</td>
</tr><tr>
  <th>Name</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG, §§ 2 ff. Immatrikulations-, Rückmelde- und Exmatrikulationssatzung der Ludwig-Maximilians-Universität München vom 11. Februar 2019 in der Fassung der Änderungssatzung vom 19. November 2019</td>
  <td>Erfüllung der öffentlichen Aufgaben der Universität, Identifikation und Ausweis, Nachweis des Studierendenstatus, Legitimationsprüfung</td>
</tr><tr>
  <th>Vorname</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG, §§ 2 ff. Immatrikulations-, Rückmelde- und Exmatrikulationssatzung der Ludwig-Maximilians-Universität München vom 11. Februar 2019 in der Fassung der Änderungssatzung vom 19. November 2019</td>
  <td>Erfüllung der öffentlichen Aufgaben der Universität, Identifikation und Ausweis, Nachweis des Studierendenstatus, Legitimationsprüfung</td>
</tr><tr>
  <th>Geburtsdatum</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG, §§ 2 ff. Immatrikulations-, Rückmelde- und Exmatrikulationssatzung der Ludwig-Maximilians-Universität München vom 11. Februar 2019 in der Fassung der Änderungssatzung vom 19. November 2019</td>
  <td>Erfüllung der öffentlichen Aufgaben der Universität, Identifizierung und Ausweis, Nachweis des Studierendenstatus; entspricht Vorgaben eines internationalen Studierendenausweises (ISIC);</td>
</tr><tr>
  <th>Kartennummer</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG, Art. 88 BayHSchG</td>
  <td>Erfüllung der öffentlichen Aufgaben des Studentenwerks, Nutzung der Mensen, der Automaten und des Kopierwesens</td>
</tr><tr>
  <th>Matrikelnummer</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG, §§ 2 ff. Immatrikulations-, Rückmelde- und Exmatrikulationssatzung der Ludwig-Maximilians-Universität München vom 11. Februar 2019 in der Fassung der Änderungssatzung vom 19. November 2019</td>
  <td>Erfüllung der öffentlichen Aufgaben der Universität, Identifikation und Ausweis, Nachweis des Studierendenstatus, Beschleunigung der Zugriffe in Fachsystemen und Prüfung von Zugangsberechtigungen</td>
</tr><tr>
  <th>Bibliotheksnummer</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG, Art. 2 Abs. 3 BayHSchG, §§ 2, 3 Allgemeine Benützungsordnung der Bayerischen Staatlichen Bibliotheken (BayABOB) vom 18. August 1993</td>
  <td>Erfüllung der öffentlichen Aufgaben der Universität, Identifikation und Ausweis, Nutzung der Bibliotheksservices</td>
</tr><tr>
  <th colspan="3">Temporäre Aufdrucke auf dem Thermo-Read-Write Streifen</th>
</tr><tr>
  <th>Gültigkeitsvermerk (gültig-bis-Datum)</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG</td>
  <td>Erfüllung der öffentlichen Aufgaben der Universität, Identifikation und Ausweis, Nachweis des Studierendenstatus, Missbrauchsschutz</td>
</tr><tr>
  <th>Fakultät(en) (in der Regel die Fakultät, der das Hauptfach des ersten Studiengangs zugeordnet ist)</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG, §§ 2 ff. Immatrikulations-, Rückmelde- und Exmatrikulationssatzung der Ludwig-Maximilians-Universität München vom 11. Februar 2019 in der Fassung der Änderungssatzung vom 19. November 2019</td>
  <td>Erfüllung der öffentlichen Aufgaben der Universität Identifikation und Ausweis, Nachweis des Studierendenstatus, Mitglied eines Studienganges, Prüfung der Zugangsberechtigung, z.B. zu den Teilbibliotheken</td>
</tr><tr>
  <th>Semesterticket</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG, Art. 2 Abs. 3 BayHSchG</td>
  <td>Erfüllung der öffentlichen Aufgaben der Universität, Identifikation und Ausweis, Nachweis des Studierendenstatus, Erfüllung eines mit der MVV GmbH geschlossenen Vertrages zur Nutzung des Semestertickets</td>
</tr>
</tbody>
</table>

Auf der Karte befindet sich ein RFID-Chip mit Datenspeicher, auf dem folgende Merkmale in unterschiedlichen Segmenten gespeichert sind:

- Matrikelnummer
- Bibliotheksnummer
- Kartentyp (bei Studierenden immer „Student")
- Kartennummer
- Gültigkeitsvermerk (gültig-bis-Datum)
- eduPersonPrincipalName (LMU-ID@lmu.de)

Die gespeicherten Daten sind zugriffsgeschützt und können nur mit entsprechend konfigurierten Kartenlesern ausgelesen werden. Die Nutzung der gespeicherten Daten unterliegt den einschlägigen rechtlichen Vorgaben, insbesondere zum Datenschutz und zur Datensicherheit.
Im Rahmen der LMUcard-Verwaltung werden folgende weitere Daten auf Servern der LMU unter der Administration des Dezernats VI und unter Beachtung der geeigneten technischen und organisatorischen Sicherheitsmaßnahmen gespeichert:

- Daten zu beantragten Karten (Datum des Antragsdaten sowie die o.g. Daten)
- Daten zu bereits ausgestellten Karten

Die Daten sind für die Nutzung und Sicherheit der LMUcard erforderlich.

<table>
<thead>
<tr>
<th>Datum</th>
<th>Rechtsgrundlage</th>
<th>Zweck</th>
</tr>
</thead>
<tbody>
<tr>
  <th>Matrikelnummer</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG, §§ 2 ff. Immatrikulations-, Rückmelde- und Exmatrikulationssatzung der Ludwig-Maximilians-Universität München vom 11. Februar 2019 in der Fassung der Änderungssatzung vom 19. November 2019</td>
  <td>Erfüllung der öffentlichen Aufgaben der Universität, Identifikation und Ausweis, Nachweis des Studierendenstatus; Beschleunigung der Zugriffe in Fachsystemen und Prüfung von Zugangsberechtigungen</td>
</tr><tr>
  <th>Bibliotheksnummer</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG, §§ 2, 3 Allgemeine Benützungsordnung der Bayerischen Staatlichen Bibliotheken (BayABOB) vom 18. August 1993</td>
  <td>Erfüllung öffentlicher Aufgaben der Universität, Identifikation und Ausweis, Nutzung der Bibliotheksservices</td>
</tr><tr>
  <th>Kartentyp (bei Studierenden immer „Student")</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG, §§ 2 ff. Immatrikulations-, Rückmelde- und Exmatrikulationssatzung der Ludwig-Maximilians-Universität München vom 11. Februar 2019 in der Fassung der Änderungssatzung vom 19. November 2019</td>
  <td>wird aktuell nicht aktiv genutzt, könnte aber z.B. zusammen mit Gültigkeitsdatum zur automatisierten Zugangskontrolle von Gebäuden (z.B. Bibliotheken) oder Räumen genutzt werden</td>
</tr><tr>
  <th>Kartennummer</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG, Art. 88 BayHSchG</td>
  <td>Erfüllung der öffentlichen Aufgaben des Studentenwerks, Nutzung der Mensen, der Automaten und des Kopierwesens</td>
</tr><tr>
  <th>Gültigkeitsvermerk (gültig-bis-Datum)</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG</td>
  <td>Erfüllung der öffentlichen Aufgaben der Universität, Identifikation bzw. Nachweis des Studierendenstatus, Missbrauchsschutz</td>
</tr><tr>
  <th>eduPersonPrincipalName (LMU-ID@lmu.de)</th>
  <td>Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 42 Abs. 4 Satz 1 BayHSchG i.V.m. Art. 4 Abs. 1 BayDSG</td>
  <td>Erfüllung der öffentlichen Aufgaben der Universität, Identifikationsmerkmal bei Zugriff auf Daten im Identity -Management zum Abruf weiterer Daten, bei entsprechender Berechtigung und Erforderlichkeit</td>
</tr>
</tbody>
</table>

Der Upload eines Fotos ist freiwillig.
Die Akzeptanz als internationaler Studierendenausweis analog zur ISIC ist meist nur mit Foto gegeben.

Die Verarbeitung von akademischen Graden und Titel erfolgt im Rahmen der Erfüllung der öffentlichen Aufgaben der Universität, soweit die Verarbeitung gesetzlich vorgeschrieben oder der Titel Namensbestandteil ist. Im Übrigen erfolgt der Aufdruck freiwillig.

Die Unterzeichnung auf dem Studierendenausweis ist ein Gültigkeitsmerkmal und dient dem Missbrauchsschutz.
Die Datenverarbeitung erfolgt insoweit zur Erfüllung einer öffentlichen Aufgabe gemäß Art. 6 Abs. 1 lit. e DSGVO i.V.m. Art. 4 Abs. 1 BayDSG.
Die Unterschrift dient auch der Personalisierung und der Akzeptanz im Rechtsverkehr.
Ohne Unterschrift ist auch die Akzeptanz als internationaler Studierendenausweis analog zur ISIC eingeschränkt bzw. nicht gegeben ist.

Die Verarbeitung der Daten erfolgt auf internen Datenverarbeitungsanlagen.
Innerhalb der LMU erhalten nur diejenigen Personen bzw. Bereiche Zugriff auf personenbezogene Daten, die diese für die Erfüllung der genannten Verarbeitungszwecke benötigen.
Die entsprechenden Personen unterliegen dem Datengeheimnis gemäß Art. 11 BayDSG.

### Datenübermittlung

Eine unbefugte Weitergabe der erhobenen und verarbeiteten Daten an Dritte, internationale Organisationen oder in ein Drittland erfolgt nicht.

### Dauer der Speicherung der personenbezogenen Daten

Die gespeicherten Daten werden zum Zweck der Erstellung der LMUcard verarbeitet, solange und soweit dies erforderlich ist (Art. 4 Abs. 1 BayDSG) bzw. eine Einwilligung vorliegt.

Nach Exmatrikulation oder nach Rückgabe der LMUcard werden die betreffenden Daten unverzüglich aus den entsprechenden Systemen gelöscht, sofern und soweit dem keine gesetzlichen Aufbewahrungspflichten (z.B. im Hinblick auf die Studierendendaten gemäß Art. 6 Abs. 1 BayArchivG) oder andere Verarbeitungszwecke (z.B. Art. 6 Abs. 2 BayDSG) entgegenstehen.
Das gilt auch für die beim Studentenwerk, der Universitätsbibliothek und der Bayerischen Staatsbibliothek gespeicherten Daten.

Das hochgeladene Foto wird ausschließlich zur Erstellung der LMUcard verarbeitet und sieben Tage nach Aushändigung der LMUcard bzw. nach Abbruch des Kartenantrags oder bei Widerruf gelöscht.
Der Fotoaufdruck wird bei Ausgabe der LMUcard an den Berechtigten kontrolliert.
Der Aufdruck von akademischen Graden und Titel ist freiwillig, soweit die Datenverarbeitung nicht aufgrund Gesetz erfolgt.

Soweit eine Datenverarbeitung auf einer Einwilligung beruht, erfolgt die Löschung mit Widerruf der Einwilligung.

Nach Rückgabe der LMUcard wird die Karte datenschutzkonform vernichtet.

### Gebühren

Die Ausstellung der LMUcard ist für die Studierenden kostenlos. Für die Ausstellung einer Ersatzkarte (bspw. bei Verlust) kann eine Bearbeitungsgebühr erhoben werden.

### Verantwortlicher, zuständige Dienststelle und Support

#### Verantwortlicher für die Datenverarbeitung

<address>
Ludwig-Maximilians-Universität München
gesetzlich vertreten durch den Präsidenten, Herrn Prof. Dr. Bernd Huber,
Geschwister-Scholl-Platz 1
80539 München
</address>

#### Zuständige Dienststelle für die Datenverarbeitung

<address>
Ludwig-Maximilians-Universität München<br/>
Dezernat VI - Informations- und Kommunikationstechnik<br/>
Geschwister-Scholl-Platz 1<br/>
80539 München
</address>
<VI@verwaltung.uni-muenchen.de>

#### Allgemeine Fragen und Support

<address>
Ludwig-Maximilians-Universität München<br/>
Dezernat VI - Informations- und Kommunikationstechnik<br/>
-- IT-Servicedesk --<br/>
Geschwister-Scholl-Platz 1<br/>
80539 München<br/>
</address>
<it-servicedesk@lmu.de>

### Kontaktdaten des behördlichen Datenschutzbeauftragten der LMU

<address>
Ludwig-Maximilians-Universität München<br/>
-- Behördlicher Datenschutzbeauftragter --<br/>
Geschwister-Scholl-Platz 1<br/>
80539 München <br/>
https://www.lmu.de/datenschutz<br/>
</address>
<datenschutz@verwaltung.uni-muenchen.de>

### Information zu bestehenden Rechten

Bei Vorliegen der gesetzlichen Voraussetzungen stehen Ihnen folgende Rechte zu:

Sie haben das Recht, Auskunft über die zu Ihrer Person gespeicherten Daten zu erhalten (Art. 15 DSGVO). Sollten unrichtige personenbezogene Daten verarbeitet werden, steht Ihnen ein Recht auf Berichtigung zu (Art. 16 DSGVO). Zudem haben Sie das Recht auf Löschung (Art. 17 DSGVO), Widerspruch (Art. 21 DSGVO), Beschränkung (Art. 18 DSGVO) und auf Widerruf einer Einwilligung für die Zukunft. Die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Datenverarbeitung wird durch diesen nicht berührt. Daneben steht Ihnen gemäß Art. 77 DSGVO das Recht zur Beschwerde bei einer Datenschutzaufsichtsbehörde zu. Die für die Ludwig-Maximilians-Universität München zuständige Datenschutz-Aufsichtsbehörde ist der Bayerische Landesbeauftragte für den Datenschutz (Postfach 22 12 19, 80502 München, Telefon: 089 212672-0, Telefax: 089 212672-50, E-Mail: <poststelle@datenschutz-bayern.de>, Internet: https://www.datenschutz-bayern.de).

Sollten Sie von Ihren Rechten Gebrauch machen wollen oder Fragen haben, wenden Sie sich bitte an die zuständige Dienststelle für die Datenverarbeitung.
Diese prüft, ob die gesetzlichen Voraussetzungen erfüllt sind und trifft dann die erforderlichen Maßnahmen.

### Geltung

Es gelten die Nutzungshinweise und Datenschutzinformationen in der aktuellen Fassung.
Diese sind abrufbar unter https://www.portal.uni-muenchen.de/benutzerkonto/index.html\#!/LMUcard/about/StudentID/termsofuse
